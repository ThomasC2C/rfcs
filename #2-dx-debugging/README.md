- Started date: 22/04/2021
- Relevant team: Nailie backend team

---

# RFC#2 Improve DX and debugging

Request For Comment

## Summary

The RFC aims to improve DX, adjusting the errors logging and error output format and hot reload of the development environment.

## Motivation

The current error handler provides a partial error logging. Errors from a deep element’s stack are not revealed, making the debugging process harder and the features development slower.

# Details design

## Deep stack error

Example:

```js
// /some/where/deep/in/app/file.js
const SERVICE_NAME = 'custom-tracer';

const tracer = () => {
  console.log('trace me', unfoundDependencie.undefinedMethod());
};

module.exports = tracer;
```

Gives

```shell
error: Failed running cloud function getMyNotificationsV3 for user r03jvyfklm with:Input: {"bookingId":"qH1r064U7E"}Error: {"message":"unfoundDependencie is not defined","code":141} {"functionName":"getMyNotificationsV3","error":{"message":"unfoundDependencie is not defined","code":141},"params":{"bookingId":"qH1r064U7E"},"user":"r03jvyfklm"}error: Parse error: unfoundDependencie is not defined {"code":141,"stack":"Error: unfoundDependencie is not defined\n at error (/home/thomas/Lab/c2c/nailie/nailie-api/node_modules/parse-server/lib/Routers/FunctionsRouter.js:121:16)\n at processTicksAndRejections (internal/process/task_queues.js:93:5)"}
```

## Format output log

Format output the error log brings readability.

Example:

```shell
"error": "Failed running cloud function getMyNotificationsV3 for user r03jvyfklm with":"input": {"bookingId":"qH1r064U7E"}"error": {"message":"unfoundDependencie is not defined","code":141}{"functionName": "getMyNotificationsV3","error":{"message":"unfoundDependencie is not defined","code":141},"params":{"bookingId":"qH1r064U7E"},"user":"r03jvyfklm"}"error":"Parse error":"unfoundDependencie is not defined"{"code":141,"stack":"Error: unfoundDependencie is not defined at error (/home/thomas/Lab/c2c/nailie/nailie-api/node_modules/parse-server/lib/Routers/FunctionsRouter.js:121:16)at processTicksAndRejections (internal/process/task_queues.js:93:5)"}
```

## Hot Reload

On updating codes, reload the server

## Prettier

Make the code looking same for every developer:

- One team.
- One project.
- One writting rule.

## ES6

Make it EX6 compliant

## Typescript

Make interfaces and type of the lower abstraction layer