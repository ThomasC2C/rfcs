- Started date: 11/05/2021
- Relevant team: Nailie backend team

---

# RFC#4 Modules unification

## Summary

The RFC aims to define the Nailie's file structure arround `modules` mounted within a `lifecycle`.

## Motivation

Nailie's API is the leader on its market. It has changed from a _one-guy-team_ to a profitable multi-teams project. This proposal intends to bring [S.O.L.I.D._ity_](https://en.wikipedia.org/wiki/SOLID) and confidence in its development . Because files like `./cloud/modules/Booking.js` has more than 6k lines, [developers rely on the file structure](https://blog.cleancoder.com/uncle-bob/2011/09/30/Screaming-Architecture.html) to understand the purpose of Nailie.

## Detail design

1. `lifecycle` on booting app
2. unify file structure by `modules`
3. bring `Nailie` to developers

### 1. lifecycle on booting

1. before express, such as injecting config
2. express context, such as mutate Parse
3. parse context, such as cloud functions

### 2. Module unification

App structure

- `/config` environmnent-_ilized_ configurations
- `/initializers` contains main context level functions
- `/helpers` contains pure functions
- `/modules` contains business details
- `/policies` contains rule to grant access to modules
- `cluster.js`
- `server.js`

Dependencies

- `/node_modules/@nailie/core/resolver` parse directory and inject components
- `/node_modules/@nailie/core/boostrap` contains server bootstraping abstracts
- `/node_modules/@nailie/parse/migrate` contains parse migration
- `/node_modules/@nailie/parse/cloud` contains parse migration

/modules/{module-name}

- `/clouds` cloud functions from https://{URL}/api/functions/{module-name}
- `/helpers` pure functions
- `/jobs` scheduled functions
- `/policies`
- `/routes` routed functions https://{URL}/{module-name}
- `config.js` contains configurations
- `const.js` constants, Nailie['module-name'].CONST
- `i18n.js` internationalization
- `model.js` module's model

### 3. Nailie

`global.Nailie.clouds['module-name']` cloud functions
`global.Nailie.config['module-name']` configuration
`global.Nailie.helpers['module-name']` pure functions
`global.Nailie.jobs['module-name']` schedulable functions
`global.Nailie.policies['module-name']` rules grantings clouds/routes
`global.Nailie.routes['module-name']` middlewares
`global.Nailie.i18n['module-name']` internationalization
`global.Nailie.model['module-name']`
