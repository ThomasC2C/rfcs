- Started date: 22/04/2021
- Relevant team: Nailie backend team

---

# RFC#3 middleware and parse-policies

## Summary

Adding middlewares within Nailie's server and policies within Parser-server enables developers to hook with ease on any Cloud Functions. It keeps the code clean, readable and maintainable.

## Motivation

Because it's hard to hook after Parse-server intialisation (after its own middleware) but before Functions execution time, Nailie's cloud functions repeat lots of code. Making harder to understand and maintain.

### Repeat controlling

```js
// in/any/cloud/function.js
Utils.logRequest(request);

if (_.isUndefined(request.params.userId) || request.params.userId === '')

if (_.isUndefined(request.user) || request.user.get('status') != CONSTANT.USER_STATUS.ACTIVE)

```

### Repeat definition

```js
'use strict';

/* --- Cloud Code hook to do this for User class --- */
const UserCloud = require('./modules/User.js');

Parse.Cloud.beforeSave(Parse.User, UserCloud.beforeSaveUser);
Parse.Cloud.afterSave(Parse.User, UserCloud.afterSaveUser);
Parse.Cloud.define('deleteUserById', UserCloud.deleteUserById);
// ...
```

### Repeat querying

```js
NailieBaseQuery.getInstallationQuery = function (isUsedCache) {
  let query = new Parse.Query(Parse.Installation);

  if (typeof isUsedCache !== 'undefined' && isUsedCache === true) {
    NailieBaseQuery.getInstallationQuery = function () {
      return query;
    };
  }

  return query;
};

NailieBaseQuery.getPostViewQuery = function (isUsedCache) {
  let query = new Parse.Query('PostView');

  if (typeof isUsedCache !== 'undefined' && isUsedCache === true) {
    NailieBaseQuery.getPostViewQuery = function () {
      return query;
    };
  }

  return query;
};
```

# Detail design

Here is an experimental files structure which brings lifecycles throught Nailie's server.

> No breaking changes
> It's fully compatible with the current version.
>
> This is an experiemental implementation of [RFC#4 Modules unification](https://bitbucket.org/ThomasC2C/rfcs/src/master)

## `./app`

- `/.core` abstraction layer
- `/config` application configuration
- `/initializsers` inject context before `app`
- `/middlewares` interact with `app`
- `/parse-policies` interact with `Parse app`
- `/routes` set `ejs`templating engine
- `parse-policies.js` rules between `Parse Cloud` and `parse-policies`
